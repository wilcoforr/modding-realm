﻿//IIFE to populate copyright footer date
(function() {
    //update footer with last modified date
    let dateString = document.lastModified.split(' ')[0]; //get date like MM/DD/YYYY
    let docLastUpdatedText = "Document last updated at: " + dateString; 

    docLastUpdatedText += "<br>";

    docLastUpdatedText += "Copyright " + new Date().getFullYear() + " by <a href='https://gitlab.com/wilcoforr'>wilcoforr</a>";

    document.getElementById("footerText").innerHTML = docLastUpdatedText;
})();

